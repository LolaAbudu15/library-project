import java.util.ArrayList;

public class Library {
    
    private ArrayList<LibraryModel> libraryItems = new ArrayList<>();

    public Library(){}

    public void returnAllLibraryItems(){
        System.out.println("All Library Items: ");
        for(LibraryModel items : this.libraryItems){
            System.out.println(" Title: " + items.getTitle() + " Number of Copies: "
            + items.getNumOfCopies() + " Item type " + items.getType());
        }
    }

    public boolean checkIfAvailable(LibraryModel item){
        if(this.libraryItems.contains(item)){
            System.out.println("Congrats, you can check " + item.getTitle() + " out");
        return true;
        }
        System.out.println("Sorry there are no more copies of " + item.getTitle() + " remaining");
        return false;
    }
    
    public void addLibraryItem(LibraryModel item){
        this.libraryItems.add(item);
        System.out.println(item.getTitle() + " got added");
    }

    public void removeLibraryItem(LibraryModel item){
        this.libraryItems.remove(item);
        System.out.println(item.getTitle() + " got removed");
    }

    public void checkItemOut(LibraryModel item){
        if(item.getType().equals("Periodical")){
            System.out.println("Sorry you cannot take " + item.getTitle() + " out the library");
            return;
        }else if(this.libraryItems.contains(item)){
            System.out.println("Congrats you have checked out " + item.getTitle());
            return;
        }System.out.println("Sorry " + item.getTitle() + " does not exist in Library");
    }

    public void returnItem(LibraryModel item){
        if(this.libraryItems.get(this.libraryItems.indexOf(item)).returnItem()){
            System.out.println(item.getTitle() + " has been successfully returned to Library");
        }else 
         System.out.println(item.getTitle() + " cannot get returned to the Library");
    }

}