
public class TestLibrary {
    public static void main(String[] args) {
        Library myLibrary = new Library();

        //add Books, CDs, DVDs, and Periodical to Library
        myLibrary.addLibraryItem(new Book("Book1 title", 5, "Book1 Author", 50));
        myLibrary.addLibraryItem(new Book("Book2 title", 10, "Book2 Author", 60));
        myLibrary.addLibraryItem(new Book("Book3 title", 15, "Book3 Author", 70));
        myLibrary.addLibraryItem(new Book("Book4 title", 20, "Boo4 Author", 80));

        myLibrary.addLibraryItem(new CD("CD1 title", 1, "CD1 Artist"));
        myLibrary.addLibraryItem(new CD("CD2 title", 2, "CD2 Artist"));
        myLibrary.addLibraryItem(new CD("CD3 title", 3, "CD3 Artist"));
        myLibrary.addLibraryItem(new CD("CD4 title", 4, "CD4 Artist"));

        myLibrary.addLibraryItem(new DVD("DVD1 title", 2, 50));
        myLibrary.addLibraryItem(new DVD("DVD2 title", 4, 100));
        myLibrary.addLibraryItem(new DVD("DVD3 title", 6, 150));
        myLibrary.addLibraryItem(new DVD("DVD4 title", 8, 200));

        myLibrary.addLibraryItem(new Periodical("Periodical1 title", 3, "Periodical1 publisher"));
        myLibrary.addLibraryItem(new Periodical("Periodical2 title", 6, "Periodical2 publisher"));
        myLibrary.addLibraryItem(new Periodical("Periodical3 title", 9, "Periodical3 publisher"));
        myLibrary.addLibraryItem(new Periodical("Periodical4 title", 12, "Periodical4 publisher"));

        //returns all Items in Library
        myLibrary.returnAllLibraryItems();

        //returns the item(Book) being borrowed from Library plus the updated inventory
        LibraryModel borrowItem = new Book("Book1 title", 5, "Book1 Author", 50);
        myLibrary.checkItemOut(borrowItem);
        myLibrary.returnAllLibraryItems();

        //returns the item(Book) that does not exist in Library
        LibraryModel borrowItemUnexists = new Book("Book5 title", 5, "Book5 Author", 50);
        myLibrary.checkItemOut(borrowItemUnexists);
        myLibrary.returnAllLibraryItems();

        //returns the item(Book) being returned back to Library plus the updated inventory
        LibraryModel returnItem = new Book("Book1 title", 5, "Book1 Author", 50);
        myLibrary.returnItem(returnItem);
        myLibrary.returnAllLibraryItems();

        //returns inability to borrow Periodical
        LibraryModel periodical = new Periodical("Periodical1 title", 3, "Periodical1 publisher");
        myLibrary.checkItemOut(periodical);
        
    }
}