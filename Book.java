public class Book extends LibraryModel {

    private String author;
    private int numOfPages;

    public Book(String title, int numOfCopies, String author, int numOfPages) {
        super(title, numOfCopies);
        this.author = author;
        this.numOfPages = numOfPages;
        this.setType("Book");
    }

    public String getAuthor() {
        return author;
    }

    public int getNumOfPages() {
        return numOfPages;
    }
}