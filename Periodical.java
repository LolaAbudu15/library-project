public class Periodical extends LibraryModel {

    private String publisher;

    public Periodical(String title, int numOfCopies, String publisher) {
        super(title, numOfCopies);
        this.publisher = publisher;
        this.setType("Periodical");
    }

    public String getPublisher() {
        return publisher;
    }   
}