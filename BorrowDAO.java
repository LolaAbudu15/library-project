public interface BorrowDAO {

    boolean borrowItem();
    boolean returnItem();
    
}