public class CD extends LibraryModel {
    private String artist;

    public CD(String title, int numOfCopies, String artist) {
        super(title, numOfCopies);
        this.artist = artist;
        this.setType("CD");
    }

    public String getArtist() {
        return artist;
    }  
}