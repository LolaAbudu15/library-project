public class LibraryModel implements BorrowDAO {
    private String title;
    private int numOfCopies;
    private String type;

    public LibraryModel(String title, int numOfCopies) {
        this.title = title;
        this.numOfCopies = numOfCopies;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumOfCopies() {
        return numOfCopies;
    }

    public void setNumOfCopies(int numOfCopies) {
        this.numOfCopies = numOfCopies;
    }

    public int subtractItem(){
        this.numOfCopies--;
        return numOfCopies;
    }

    public int addItem(){
        this.numOfCopies++;
        return numOfCopies;
    }

    @Override
    public boolean borrowItem() {
        if(this.getNumOfCopies() > 0){
            subtractItem();
            System.out.println("Congrats you can borrow this item! There are "
            +subtractItem() + " remaining" );
            return true;
        }else{
            System.out.println("Sorry there are " + subtractItem() + " copies of this item left");
        return false;
        }
    }

    @Override
    public boolean returnItem() {
        addItem();
        System.out.println("There are " + addItem() + " of these items remaining");
        return false;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    // public boolean copiesRemaining(LibraryModel item){
    //     if(item.getNumOfCopies() > 0){
    //         return true;
    //     } else{
    //         System.out.println("Sorry there are no copies of " + item + " left!");
    //         return false;
    //     }
    // }
}