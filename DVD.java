public class DVD extends LibraryModel {

  private int dvdLength;

  public DVD(String title, int numOfCopies, int dvdLength) {
      super(title, numOfCopies);
      this.dvdLength = dvdLength;
      this.setType("DVD");
  }

  public int getDvdLength() {
      return dvdLength;
  }
}